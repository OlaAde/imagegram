package com.bandlab.imagegram.service;

import com.bandlab.imagegram.dto.PostCommentCreationPayloadDto;
import com.bandlab.imagegram.dto.PostCommentDto;
import com.bandlab.imagegram.entity.Comment;
import com.bandlab.imagegram.entity.Post;
import com.bandlab.imagegram.exception.OperationNotPermittedException;
import com.bandlab.imagegram.exception.ResourceNotFoundException;
import com.bandlab.imagegram.repository.CommentRepository;
import com.bandlab.imagegram.repository.PostRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

    @InjectMocks
    private CommentService commentService;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private PostRepository postRepository;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreatePostComment_Success() throws ResourceNotFoundException {
        String postId = "post-1";
        Post post = new Post(null);
        post.setId(postId);
        PostCommentCreationPayloadDto payload = new PostCommentCreationPayloadDto();
        payload.setContent("This is a test comment");

        when(postRepository.findById(postId)).thenReturn(Optional.of(post));
        when(commentRepository.save(any(Comment.class))).thenAnswer(invocation -> invocation.getArgument(0));

        PostCommentDto commentDto = commentService.createPostComment(postId, payload);

        ArgumentCaptor<Comment> commentCaptor = ArgumentCaptor.forClass(Comment.class);
        verify(commentRepository).save(commentCaptor.capture());

        verify(postRepository).save(post);

        Comment savedComment = commentCaptor.getValue();
        assertEquals(postId, savedComment.getPostId());
        assertEquals("This is a test comment", savedComment.getContent());
        assertEquals(Optional.of(1), post.getTotalNumberOfComments());
        assertEquals(1, post.getLatestComments().size());
        assertEquals(savedComment, post.getLatestComments().get(0));
        assertEquals(savedComment, commentDto.build(savedComment));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testCreatePostComment_PostNotFound() throws ResourceNotFoundException {
        String postId = "post-1";
        PostCommentCreationPayloadDto payload = new PostCommentCreationPayloadDto();

        when(postRepository.findById(postId)).thenReturn(Optional.empty());

        commentService.createPostComment(postId, payload);
    }

    @Test
    public void testDeleteComment_Success() throws ResourceNotFoundException, OperationNotPermittedException {
        String postId = "post-1";
        String commentId = "comment-1";
        Comment comment = new Comment(postId, "comment-content");
        Post post = new Post(null);
        post.setId(postId);
        post.setTotalNumberOfComments(2);
        List<Comment> comments = new ArrayList<>();
        comments.add(comment);
        post.setLatestComments(comments);

        when(commentRepository.findByPostIdAndId(postId, commentId)).thenReturn(Optional.of(comment));
        when(postRepository.findById(postId)).thenReturn(Optional.of(post));

        commentService.deleteComment(postId, commentId);

        verify(commentRepository).deleteById(commentId);
        verify(postRepository).save(post);

        assertEquals(Optional.of(1), post.getTotalNumberOfComments());
        assertTrue(post.getLatestComments().isEmpty());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDeleteComment_CommentNotFound() throws ResourceNotFoundException, OperationNotPermittedException {
        String postId = "post-1";
        String commentId = "comment-1";

        when(commentRepository.findByPostIdAndId(postId, commentId)).thenReturn(Optional.empty());

        commentService.deleteComment(postId, commentId);
    }

    @Test(expected = OperationNotPermittedException.class)
    public void testDeleteComment_OperationNotPermitted() throws ResourceNotFoundException, OperationNotPermittedException {
        String postId = "post-1";
        String commentId = "comment-1";
        Comment comment = new Comment(postId, "comment-content");

        when(commentRepository.findByPostIdAndId(postId, commentId)).thenReturn(Optional.of(comment));

        Authentication authentication = new UsernamePasswordAuthenticationToken("user2", "password");
        SecurityContextHolder.getContext().setAuthentication(authentication);

        commentService.deleteComment(postId, commentId);
    }
}

