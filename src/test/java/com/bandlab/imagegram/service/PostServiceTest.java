package com.bandlab.imagegram.service;


import com.bandlab.imagegram.dto.PostCreationDto;
import com.bandlab.imagegram.dto.PostWithCommentsDto;
import com.bandlab.imagegram.entity.Post;
import com.bandlab.imagegram.entity.PostImageUploadStatus;
import com.bandlab.imagegram.repository.PostRepository;
import com.bandlab.imagegram.service.AwsS3Service;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class PostServiceTest {

    @InjectMocks
    private PostService postService;

    @Mock
    private PostRepository postRepository;

    @Mock
    private AwsS3Service awsS3Service;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreatePost_Success() throws IOException, InterruptedException, ExecutionException {
        MultipartFile image = createMockMultipartFile("test.jpg");
        String caption = "Test post caption";
        Post post = new Post(null);
        post.setId("post-1");

        when(postRepository.save(any(Post.class))).thenReturn(post);

        CompletableFuture<Void> uploadFuture = CompletableFuture.completedFuture(null);
        when(awsS3Service.uploadOriginalFile(post.getId(), image)).thenReturn(uploadFuture);

        String originalImageUrl = "https://original-image-url";
        String convertedImageUrl = "https://converted-image-url";
        when(awsS3Service.getFutureFileUrlForOriginalImage(post.getId(), image)).thenReturn(originalImageUrl);
        when(awsS3Service.getFutureFileUrlForConvertedImage(post.getId())).thenReturn(convertedImageUrl);

        CompletableFuture<PostCreationDto> creationFuture = postService.createPost(image, caption);

        verify(postRepository).save(any(Post.class));
        verify(awsS3Service).uploadOriginalFile(post.getId(), image);

        PostCreationDto creationDto = creationFuture.get();
        assertEquals("post-1", creationDto.getId());

        ArgumentCaptor<Post> postCaptor = ArgumentCaptor.forClass(Post.class);
        verify(postRepository).save(postCaptor.capture());
        Post savedPost = postCaptor.getValue();
        assertEquals(originalImageUrl, savedPost.getOriginalImageURl());
        assertEquals(convertedImageUrl, savedPost.getConvertedImageUrl());
    }

    @Test
    public void testGetPosts_Success() {
        Pageable pageable = PageRequest.of(0, 10);
        List<Post> posts = createTestPosts(5);
        Page<Post> page = new PageImpl<>(posts, pageable, 5);
        when(postRepository.findAllByOrderByTotalNumberOfCommentsDesc(pageable)).thenReturn(page);

        Page<PostWithCommentsDto> result = postService.getPosts(pageable);

        verify(postRepository).findAllByOrderByTotalNumberOfCommentsDesc(pageable);

        assertEquals(5, result.getContent().size());
        assertEquals(5, result.getTotalElements());
    }

    @Test
    public void testHandleUploadCompletion_UploadFailed() {
        Throwable exception = new IOException("Upload failed");
        Post post = new Post(null);
        post.setId("post-1");

        postService.handleUploadCompletion(exception, post.getId());

        ArgumentCaptor<Post> postCaptor = ArgumentCaptor.forClass(Post.class);
        verify(postRepository).save(postCaptor.capture());

        Post savedPost = postCaptor.getValue();
        assertEquals(PostImageUploadStatus.ORIGINAL_IMAGE_UPLOAD_FAILED, savedPost.getImageStatus());
    }

    @Test
    public void testHandleUploadCompletion_UploadSuccessful() {
        Post post = new Post(null);
        post.setId("post-1");

        postService.handleUploadCompletion(null, post.getId());

        ArgumentCaptor<Post> postCaptor = ArgumentCaptor.forClass(Post.class);
        verify(postRepository).save(postCaptor.capture());

        Post savedPost = postCaptor.getValue();
        assertEquals(PostImageUploadStatus.PENDING_CONVERTED_IMAGE_UPLOAD, savedPost.getImageStatus());
    }

    private MultipartFile createMockMultipartFile(String filename) throws IOException {
        byte[] content = new byte[]{0x01, 0x02, 0x03};
        return new MockMultipartFile("file", filename, MediaType.IMAGE_JPEG_VALUE, content);
    }

    private List<Post> createTestPosts(int count) {
        List<Post> posts = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            Post post = new Post(null);
            post.setId("post-" + i);
            posts.add(post);
        }
        return posts;
    }
}
