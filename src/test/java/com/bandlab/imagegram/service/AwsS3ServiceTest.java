package com.bandlab.imagegram.service;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@RunWith(MockitoJUnitRunner.class)
public class AwsS3ServiceTest {

    @InjectMocks
    private AwsS3Service awsS3Service;

    @Mock
    private S3Client s3Client;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testUploadOriginalFile() throws Exception {
        String imageId = "image-1";
        MultipartFile multipartFile = createMockMultipartFile("test.jpg");

        ArgumentCaptor<PutObjectRequest> putObjectRequestCaptor = ArgumentCaptor.forClass(PutObjectRequest.class);
        doNothing().when(s3Client).putObject(putObjectRequestCaptor.capture(), any(RequestBody.class));

        CompletableFuture<Void> uploadFuture = awsS3Service.uploadOriginalFile(imageId, multipartFile);

        PutObjectRequest putObjectRequest = putObjectRequestCaptor.getValue();
        assertEquals("original-images-bucket", putObjectRequest.bucket());
        assertEquals("image-1.jpg", putObjectRequest.key());

        uploadFuture.get();
    }

    @Test
    public void testGetFileExtension() throws IOException {
        MultipartFile multipartFile = createMockMultipartFile("test.jpg");

        String extension = awsS3Service.getFileExtension(multipartFile);

        assertEquals("jpg", extension);
    }

    @Test
    public void testGetFutureFileUrlForOriginalImage() throws IOException {
        String imageId = "image-1";
        MultipartFile multipartFile = createMockMultipartFile("test.jpg");

        String fileUrl = awsS3Service.getFutureFileUrlForOriginalImage(imageId, multipartFile);

        assertEquals("https://original-images-bucket.s3.amazonaws.com/image-1.jpg", fileUrl);
    }

    @Test
    public void testGetFutureFileUrlForConvertedImage() {
        String imageId = "image-1";

        String fileUrl = awsS3Service.getFutureFileUrlForConvertedImage(imageId);

        assertEquals("https://converted-images-bucket.s3.amazonaws.com/image-1.jpg", fileUrl);
    }

    private MultipartFile createMockMultipartFile(String filename) {
        byte[] content = new byte[]{0x01, 0x02, 0x03};
        return new MockMultipartFile("file", filename, MediaType.IMAGE_JPEG_VALUE, content);
    }
}

