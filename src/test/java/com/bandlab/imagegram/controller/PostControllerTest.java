package com.bandlab.imagegram.controller;

import com.bandlab.imagegram.dto.PostCreationDto;
import com.bandlab.imagegram.dto.PostWithCommentsDto;
import com.bandlab.imagegram.service.PostService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(MockitoJUnitRunner.class)
public class PostControllerTest {

    @InjectMocks
    private PostController postController;

    @Mock
    private PostService postService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(postController).build();
    }


    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testCreatePost() throws Exception {
        CompletableFuture<PostCreationDto> creationDtoFuture = CompletableFuture.completedFuture(new PostCreationDto("postid"));
        when(postService.createPost(any(MultipartFile.class), anyString())).thenReturn(creationDtoFuture);

        MockMultipartFile multipartFile = new MockMultipartFile("image", "test.jpg", "image/jpeg", new byte[0]);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/posts")
                        .file(multipartFile)
                        .param("caption", "Test Caption"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value("postid"));

        verify(postService).createPost(eq(multipartFile), eq("Test Caption"));
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testCreatePost_InternalServerError() throws Exception {
        when(postService.createPost(any(MultipartFile.class), anyString())).thenThrow(new IOException());

        MockMultipartFile multipartFile = new MockMultipartFile("image", "test.jpg", "image/jpeg", new byte[0]);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/posts")
                        .file(multipartFile)
                        .param("caption", "Test Caption"))
                .andExpect(status().isInternalServerError());

        verify(postService).createPost(eq(multipartFile), eq("Test Caption"));
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testCreatePost_SizeValidationFailure() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("image", "test.jpg", "image/jpeg", new byte[0]);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/posts")
                        .file(multipartFile))
                .andExpect(status().isBadRequest());

        verify(postService, never()).createPost(any(MultipartFile.class), anyString());
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testCreatePost_ContentTypeValidationFailure() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("image", "test.txt", "text/xml", new byte[4]);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/posts")
                        .file(multipartFile))
                .andExpect(status().isBadRequest());

        verify(postService, never()).createPost(any(MultipartFile.class), anyString());
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testGetPosts() throws Exception {
        Page<PostWithCommentsDto> posts = new PageImpl<>(Collections.emptyList());
        when(postService.getPosts(Pageable.ofSize(10))).thenReturn(posts);

        Pageable pageable = PageRequest.of(0, 10);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/posts")
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray());

        verify(postService).getPosts(eq(pageable));
    }
}
