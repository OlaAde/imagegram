package com.bandlab.imagegram.controller;

import com.bandlab.imagegram.dto.PostCommentCreationPayloadDto;
import com.bandlab.imagegram.dto.PostCommentDto;
import com.bandlab.imagegram.exception.OperationNotPermittedException;
import com.bandlab.imagegram.exception.ResourceNotFoundException;
import com.bandlab.imagegram.service.CommentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class CommentControllerTest {

    @InjectMocks
    private CommentController commentController;

    @Mock
    private CommentService commentService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(commentController)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testCreatePostComment_Success() throws Exception {
        // Prepare test data
        String postId = "post-1";
        PostCommentCreationPayloadDto payload = new PostCommentCreationPayloadDto();
        payload.setContent("This is a test comment");

        // Mock the commentService.createPostComment method
        PostCommentDto commentDto = new PostCommentDto();
        commentDto.setCreatedBy("user-1");
        commentDto.setContent("This is a test comment");
        when(commentService.createPostComment(postId, payload)).thenReturn(commentDto);

        // Perform the request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/posts/{postId}/comments", postId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(payload)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value("comment-1"))
                .andExpect(jsonPath("$.comment").value("This is a test comment"));

        // Verify that commentService.createPostComment was called with the correct arguments
        verify(commentService).createPostComment(postId, payload);
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testCreatePostComment_ResourceNotFound() throws Exception {
        // Prepare test data
        String postId = "post-1";
        PostCommentCreationPayloadDto payload = new PostCommentCreationPayloadDto();

        // Mock the commentService.createPostComment method to throw ResourceNotFoundException
        when(commentService.createPostComment(postId, payload))
                .thenThrow(new ResourceNotFoundException("Post not found"));

        // Perform the request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/posts/{postId}/comments", postId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(payload)))
                .andExpect(status().isNotFound());

        // Verify that commentService.createPostComment was called with the correct arguments
        verify(commentService).createPostComment(postId, payload);
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testDeleteComment_Success() throws Exception {
        // Prepare test data
        String postId = "post-1";
        String commentId = "comment-1";

        // Perform the request
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/posts/{postId}/comments/{commentId}", postId, commentId))
                .andExpect(status().isOk())
                .andExpect(content().string("Comment deleted successfully"));

        // Verify that commentService.deleteComment was called with the correct arguments
        verify(commentService).deleteComment(postId, commentId);
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testDeleteComment_ResourceNotFound() throws Exception {
        // Prepare test data
        String postId = "post-1";
        String commentId = "comment-1";

        // Mock the commentService.deleteComment method to throw ResourceNotFoundException
        doThrow(new ResourceNotFoundException("Comment not found")).when(commentService).deleteComment(postId, commentId);

        // Perform the request
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/posts/{postId}/comments/{commentId}", postId, commentId))
                .andExpect(status().isNotFound());

        // Verify that commentService.deleteComment was called with the correct arguments
        verify(commentService).deleteComment(postId, commentId);
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"ROLE_USER"})
    public void testDeleteComment_OperationNotPermitted() throws Exception {
        // Prepare test data
        String postId = "post-1";
        String commentId = "comment-1";

        // Mock the commentService.deleteComment method to throw OperationNotPermittedException
        doThrow(new OperationNotPermittedException()).when(commentService).deleteComment(postId, commentId);

        // Perform the request
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/posts/{postId}/comments/{commentId}", postId, commentId))
                .andExpect(status().isForbidden());

        // Verify that commentService.deleteComment was called with the correct arguments
        verify(commentService).deleteComment(postId, commentId);
    }

    // Utility method to convert object to JSON string
    private String asJsonString(Object obj) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }
}

