package com.bandlab.imagegram.validator;

import jakarta.annotation.Nullable;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;


public class ValidImageValidator implements ConstraintValidator<ValidImage, MultipartFile> {
    private static final long MAX_IMAGE_SIZE_BYTES = 104857600;
    private static final List<String> ALLOWED_IMAGE_EXTENSIONS = Arrays.asList("jpg", "jpeg", "png", "bmp");

    @Override
    public void initialize(ValidImage constraintAnnotation) {

    }

    @Override
    public boolean isValid(MultipartFile file, ConstraintValidatorContext context) {
        if (file == null || file.isEmpty()) {
            attachErrorMessageToValidatorContext(context, "Image is empty");
            return false;
        }

        if (file.getSize() > MAX_IMAGE_SIZE_BYTES) {
            attachErrorMessageToValidatorContext(context, "File is too large. Only files under 100MB can be uploaded");
            return false;
        }

        String fileExtension = getFileExtension(file.getOriginalFilename());
        if (!ALLOWED_IMAGE_EXTENSIONS.contains(fileExtension.toLowerCase())) {
            attachErrorMessageToValidatorContext(context, "Only PNG, JPG and BMP images are allowed.");
            return false;
        }

        return true;
    }

    private void attachErrorMessageToValidatorContext(ConstraintValidatorContext context, String errorMessage) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(errorMessage)
                .addConstraintViolation();
    }


    private String getFileExtension(@Nullable String filename) {
        if (filename == null) {
            return "";
        }

        int lastDotIndex = filename.lastIndexOf(".");
        if (lastDotIndex > -1 && lastDotIndex < filename.length() - 1) {
            return filename.substring(lastDotIndex + 1);
        }
        return "";
    }
}
