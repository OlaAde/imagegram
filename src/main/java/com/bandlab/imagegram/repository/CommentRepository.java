package com.bandlab.imagegram.repository;

import com.bandlab.imagegram.entity.Comment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends CrudRepository<Comment, String> {
    Optional<Comment> findByPostIdAndId(String postId, String commentId);

    List<Comment> findTop2ByOrderByCreatedAtDesc();
}
