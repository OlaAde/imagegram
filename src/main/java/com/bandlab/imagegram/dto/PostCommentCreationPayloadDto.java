package com.bandlab.imagegram.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class PostCommentCreationPayloadDto {
    @NotBlank(message = "Content must not be blank")
    private String content;
}
