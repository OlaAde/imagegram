package com.bandlab.imagegram.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class PostDto {
    private String id, imageUrl, caption, imageStatus;
    private Date createdAt;
}
