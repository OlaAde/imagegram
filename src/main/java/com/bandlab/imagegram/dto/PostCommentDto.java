package com.bandlab.imagegram.dto;

import com.bandlab.imagegram.entity.Comment;
import lombok.Data;
import org.modelmapper.ModelMapper;

import java.util.Date;

@Data
public class PostCommentDto {
    private String content, createdBy;
    private Date createdAt;

    public static PostCommentDto build(Comment comment) {
        return new ModelMapper().map(comment, PostCommentDto.class);
    }
}
