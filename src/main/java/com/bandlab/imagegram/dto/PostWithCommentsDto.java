package com.bandlab.imagegram.dto;

import com.bandlab.imagegram.entity.Post;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;


@Getter
@Setter
public class PostWithCommentsDto extends PostDto {
    private List<PostCommentDto> latestComments;

    public static PostWithCommentsDto build(Post post) {
        PostWithCommentsDto postWithCommentsDto = new ModelMapper().map(post, PostWithCommentsDto.class);
        postWithCommentsDto.setImageUrl(post.getOriginalImageURl());
        postWithCommentsDto.setLatestComments(post.getLatestComments().stream().map(PostCommentDto::build).collect(Collectors.toList()));
        return postWithCommentsDto;
    }
}