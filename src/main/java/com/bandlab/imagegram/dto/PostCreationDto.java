package com.bandlab.imagegram.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PostCreationDto {
    private String id;
}
