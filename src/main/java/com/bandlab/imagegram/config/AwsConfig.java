package com.bandlab.imagegram.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

@Configuration
public class AwsConfig {

    @Value("aws.s3.accessKey")
    private String awsAccessKey;
    @Value("aws.s3.accessKeySecret")
    private String awsSecretKey;

    @Bean
    public AwsCredentials awsCredentials() {
        return AwsBasicCredentials.create(awsAccessKey, awsSecretKey);
    }

    @Bean
    public S3Client s3Client(AwsCredentials awsCredentials) {
        return S3Client.builder()
                .region(Region.AP_NORTHEAST_1)
                .credentialsProvider(() -> awsCredentials)
                .build();
    }
}