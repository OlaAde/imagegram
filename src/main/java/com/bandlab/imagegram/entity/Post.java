package com.bandlab.imagegram.entity;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "posts")
@Getter
@Setter
public class Post {
    @Id
    private String id;

    private String originalImageURl;
    private String convertedImageUrl;
    private String caption;
    private PostImageUploadStatus imageStatus = PostImageUploadStatus.PENDING_ORIGINAL_IMAGE_UPLOAD;

    @DBRef
    private List<Comment> latestComments = new ArrayList<>();
    private Integer totalNumberOfComments = 0;

    @CreatedBy
    private String createdBy;

    @CreatedDate
    @Indexed
    private Date createdAt;

    public Post(String caption) {
        this.caption = caption;
    }
}