package com.bandlab.imagegram.entity;

import lombok.Getter;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "comments")
@Getter
public class Comment {
    @Id
    private String id;

    private String content;

    @Indexed
    private String postId;

    @CreatedDate
    @Indexed
    private Date createdAt;

    @CreatedBy
    @Indexed
    private String createdBy;


    public Comment(String postId, String content) {
        this.postId = postId;
        this.content = content;
    }
}
