package com.bandlab.imagegram.exception;

public class OperationNotPermittedException extends Exception{
    public OperationNotPermittedException() {
        super("You are not permitted to do this operation");
    }
}
