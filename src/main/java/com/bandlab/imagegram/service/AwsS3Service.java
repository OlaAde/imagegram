package com.bandlab.imagegram.service;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@RequiredArgsConstructor
public class AwsS3Service {
    @Value("aws.s3.originalImagesBucketName")
    String ORIGINAL_IMAGE_BUCKET_NAME;
    @Value("aws.s3.convertedImagesBucketName")
    String CONVERTED_IMAGE_BUCKET_NAME;

    private final S3Client s3Client;

    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    @Async
    public CompletableFuture<Void> uploadOriginalFile(String imageId, MultipartFile multipartFile) {
        CompletableFuture<Void> uploadFuture = new CompletableFuture<>();
        return CompletableFuture.runAsync(() -> {
            try {
                String extension = getFileExtension(multipartFile);

                String key = imageId + "." + extension;

                File file = File.createTempFile("temp", null);
                multipartFile.transferTo(file);

                PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                        .bucket(ORIGINAL_IMAGE_BUCKET_NAME)
                        .key(key)
                        .build();

                s3Client.putObject(putObjectRequest, RequestBody.fromFile(file));

                file.delete();
                uploadFuture.complete(null);
            } catch (IOException e) {
                uploadFuture.completeExceptionally(e);
            }
        }, executorService);
    }

    public String getFileExtension(MultipartFile multipartFile) {
        String originalFileName = multipartFile.getOriginalFilename();
        return originalFileName.substring(originalFileName.lastIndexOf('.') + 1);
    }

    public String getFutureFileUrlForOriginalImage(String imageId, MultipartFile multipartFile) {
        String extension = getFileExtension(multipartFile);
        return MessageFormat.format("https://{0}.s3.amazonaws.com/{1}.{2}", ORIGINAL_IMAGE_BUCKET_NAME, imageId, extension);
    }

    public String getFutureFileUrlForConvertedImage(String imageId) {
        String finalTargetExtension = "jpg";
        return MessageFormat.format("https://{0}.s3.amazonaws.com/{1}.{2}", CONVERTED_IMAGE_BUCKET_NAME, imageId, finalTargetExtension);
    }
}
