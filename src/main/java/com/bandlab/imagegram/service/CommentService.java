package com.bandlab.imagegram.service;

import com.bandlab.imagegram.repository.CommentRepository;
import com.bandlab.imagegram.repository.PostRepository;
import com.bandlab.imagegram.dto.PostCommentCreationPayloadDto;
import com.bandlab.imagegram.dto.PostCommentDto;
import com.bandlab.imagegram.entity.Comment;
import com.bandlab.imagegram.entity.Post;
import com.bandlab.imagegram.exception.OperationNotPermittedException;
import com.bandlab.imagegram.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final PostRepository postRepository;

    @Transactional
    public PostCommentDto createPostComment(String postId, PostCommentCreationPayloadDto payload) throws ResourceNotFoundException {
        Post post = postRepository.findById(postId).orElseThrow(() -> new ResourceNotFoundException("Post not found."));

        Comment comment = new Comment(postId, payload.getContent());
        comment = commentRepository.save(comment);

        updatePostWithLatestComment(post, comment);
        return PostCommentDto.build(comment);
    }

    private void updatePostWithLatestComment(Post post, Comment comment) {
        List<Comment> comments = post.getLatestComments();
        comments.add(comment);
        if (comments.size() > 2) {
            comments.remove(0);
        }

        post.setLatestComments(comments);
        post.setTotalNumberOfComments(post.getTotalNumberOfComments() + 1);

        postRepository.save(post);
    }

    @Transactional
    @Async
    public void deleteComment(String postId, String commentId) throws ResourceNotFoundException, OperationNotPermittedException {
        Comment comment = commentRepository.findByPostIdAndId(postId, commentId)
                .orElseThrow(() -> new ResourceNotFoundException("Comment not found."));
        Post post = postRepository.findById(postId).orElseThrow(() -> new ResourceNotFoundException("Post not found."));

        String currentUserId = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!currentUserId.contentEquals(comment.getCreatedBy())) {
            throw new OperationNotPermittedException();
        }

        CompletableFuture.runAsync(() -> {
            commentRepository.deleteById(commentId);
            updatePostForCommentRemoval(post, commentId);
        });
    }

    void updatePostForCommentRemoval(Post post, String commentId) {
        post.setTotalNumberOfComments(post.getTotalNumberOfComments() - 1);
        List<Comment> comments = post.getLatestComments();
        List<Comment> latestCommentsWithoutDeletedComment = comments.stream().filter(comment -> !comment.getId().contentEquals(commentId)).toList();

        boolean latestCommentsNeedsToBeRefreshed = latestCommentsWithoutDeletedComment.size() < 2 && post.getTotalNumberOfComments() > 1;
        if (latestCommentsNeedsToBeRefreshed) {
            post.setLatestComments(commentRepository.findTop2ByOrderByCreatedAtDesc());
        } else {
            post.setLatestComments(latestCommentsWithoutDeletedComment);
        }

        postRepository.save(post);
    }
}
