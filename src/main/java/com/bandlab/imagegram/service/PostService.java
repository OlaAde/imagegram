package com.bandlab.imagegram.service;

import com.bandlab.imagegram.dto.PostCreationDto;
import com.bandlab.imagegram.entity.PostImageUploadStatus;
import com.bandlab.imagegram.repository.PostRepository;
import com.bandlab.imagegram.dto.PostWithCommentsDto;
import com.bandlab.imagegram.entity.Post;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;
    private final AwsS3Service awsS3Service;

    @Async
    public CompletableFuture<PostCreationDto> createPost(MultipartFile image, String caption) throws IOException, InterruptedException {
        Post post = new Post(caption);
        post = postRepository.save(post);

        String postId = post.getId();

        CompletableFuture<Void> uploadFuture = awsS3Service.uploadOriginalFile(postId, image);

        uploadFuture.handleAsync((result, exception) -> handleUploadCompletion(exception, postId));

        String originalImageUrl = awsS3Service.getFutureFileUrlForOriginalImage(postId, image);
        String convertedImageUrl = awsS3Service.getFutureFileUrlForConvertedImage(postId);
        post.setOriginalImageURl(originalImageUrl);
        post.setConvertedImageUrl(convertedImageUrl);
        postRepository.save(post);

        return CompletableFuture.completedFuture(new PostCreationDto(post.getId()));
    }

    public Page<PostWithCommentsDto> getPosts(Pageable pageable) {
        return postRepository.findAllByOrderByTotalNumberOfCommentsDesc(pageable).map(PostWithCommentsDto::build);
    }

    Void handleUploadCompletion(Throwable exception, final String postId) {
        Post post = postRepository.findById(postId).get();

        if (exception != null) {
            post.setImageStatus(PostImageUploadStatus.ORIGINAL_IMAGE_UPLOAD_FAILED);
        } else {
            post.setImageStatus(PostImageUploadStatus.PENDING_CONVERTED_IMAGE_UPLOAD);
        }

        postRepository.save(post);

        return null;
    }
}
