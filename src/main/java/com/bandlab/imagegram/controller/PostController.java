package com.bandlab.imagegram.controller;

import com.bandlab.imagegram.dto.PostCreationDto;
import com.bandlab.imagegram.dto.PostWithCommentsDto;
import com.bandlab.imagegram.service.PostService;
import com.bandlab.imagegram.validator.ValidImage;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
@RequestMapping("/api/posts")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PostCreationDto> createPost(@RequestPart @ValidImage MultipartFile image,
                                                      @RequestParam(required = false) String caption) {
        try {
            PostCreationDto postCreationDto = postService.createPost(image, caption).join();
            return new ResponseEntity<>(postCreationDto, HttpStatus.CREATED);
        } catch (IOException | InterruptedException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<PostWithCommentsDto>> getPosts(Pageable pageable) {
        Page<PostWithCommentsDto> posts = postService.getPosts(pageable);
        return ResponseEntity.ok(posts);
    }
}
