package com.bandlab.imagegram.controller;

import com.bandlab.imagegram.dto.PostCommentCreationPayloadDto;
import com.bandlab.imagegram.dto.PostCommentDto;
import com.bandlab.imagegram.exception.OperationNotPermittedException;
import com.bandlab.imagegram.exception.ResourceNotFoundException;
import com.bandlab.imagegram.service.CommentService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/posts/{postId}/comments")
@AllArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PostCommentDto> createPostComment(@PathVariable String postId,
                                                            @RequestBody @Valid PostCommentCreationPayloadDto payload) {
        try {
            PostCommentDto postComment = commentService.createPostComment(postId, payload);
            return new ResponseEntity<>(postComment, HttpStatus.CREATED);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{commentId}")
    public ResponseEntity<String> deleteComment(@PathVariable String postId, @PathVariable String commentId) {
        try {
            commentService.deleteComment(postId, commentId);
            return ResponseEntity.ok("Comment deleted successfully");
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (OperationNotPermittedException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
