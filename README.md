# Routes

**NOTE: All requests are authenticated. Meaning all requests need to have the `Authorization: Bearer {authToken}` header.**

### Get posts.(Currently offset-based pagination. Instead of cursor-based)
/api/posts
* Method: GET
* Parameters: { page: number, size: number}
* response: 
The response is an offset based pagination of the posts. The content property of the response contains the list of posts with the last two comments. 
<img height="300" src="./README_IMAGES/Screenshot 2023-07-25 at 11.23.45 AM.png" width="1000"/>
<img height="300" src="./README_IMAGES/Screenshot 2023-07-25 at 11.26.29 AM.png" width="400"/>


### Create a post. Takes an image and an optional caption.

/api/posts
* Method: POST
* Parameters: image(multipart), caption(optional string)
* response: `{
  "id": "string"
  }`


### Create a comment
/api/posts/{postId}/comments
* Method: POST
* Request body: `{
  "content": "string"
  }`
* Response body: `{
  "content": "string",
  "createdBy": "string",
  "createdAt": "2023-07-25T08:48:59.071Z"
  }`

### Delete a comment 
/api/posts/{postId}/comments/{commentId}

* Method: DELETE
* Response: Comment deleted successfully



# Database Schema:
We have two collections, posts and comments.
<img src="./README_IMAGES/DataSchema.png" />

A post tracks it's total number of comments and it's last two comments.

# What's left to do:
- Switch Pagination to Cursor-based, instead of offset-based.
- Resumable image uploads. While the Multipart file allows uploading in chunks, it doesn't automatically allow the resumable upload, incase the user loses their internet connection. This needs to also be handled.
- We still need to calculate the minimum number of replicas required for the app to handle it's non-functional requirements.
- Provision a Kubernetes cluster and create a namespace specifically for the application to run in. 
- Create two aws S3 buckets 
- Create an AWS Lambda function from the Docker image gotten from the Docker stage of the ImageTransformer pipeline 
- Add the Kubernetes configuration file (kubeconfig) to the GitLab CI/CD pipeline as the variable KUBE_CONFIG_CONTENT. This allows the pipeline to interact with the Kubernetes cluster. 
- Based on the provided .gitlab-ci.yml file, add the required credentials to the GitLab CI environment variables for both repositories. These credentials include the private registry on AWS, the Kubernetes config, the two S3 buckets, the hostname for the ingress resource, the namespace, and the SSL secret.
- These credentials are essential for the proper functioning and security of the application.

# CACHING
- While this was not explored in the execution, it is a very important part of making a responsive system. Caching our posts and comments, would be very useful.
- Caching hot posts, also, meaning posts in the top percentile or with over 1000 comments can be cached.
- CDN. Since we have two s3 buckets, the s3 bucket for the converted images, the images to be saved has to be cached. 

# MONITORING
- This project showed how to deploy an AppDynamics client to monitor the traffic to the pods.
<img src="./README_IMAGES/Appdynamics.png" /> 