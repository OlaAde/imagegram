FROM amazoncorretto:17-alpine-jdk

COPY target/imagegram-0.0.1-SNAPSHOT.jar /imagegram.jar

EXPOSE 8080/tcp

ENTRYPOINT ["java", "-jar", "/imagegram.jar"]